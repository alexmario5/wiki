# Contributing to the MRCL wiki
This page will teach you how to start contributing or translating the MRCL wiki if you don't know anything about git/gitlab.
### Translation note:
This page will not be translated unless there is a very strong demand, or if someone volunteers.
# Table Of Contents
[[_TOC_]]
# Software
First we'll need some software to get going. 
## Visual Studio Code
VSCode is a popular code editor, and I recommend using it for this particular use case. You can also use Notepad++ or anything else if you really want to, but this is the easiest for beginners.

You can download it [here](https://code.visualstudio.com/download).


## Git
Git is the software that tracks all the different versions of the wiki.
You can download it [here](https://git-scm.com/downloads)

You can install it with all the default settings **except the default editor**. On that screen you should be able to select Visual Studio Code.

# Downloading the wiki
Now we'll learn how to get a copy of the wiki on our computer.

## Understanding Git
**If you think you understand git, click [here](#forking-the-repository) to skip this section**

A git repository is where code (or in this case, our wiki) is stored. It also contains all previous versions of the code, so you can see who changed what.

### Cloning
Cloning a repository means that you download it to your own computer. This will automatically link it to the online version. You can [Push](#pushing) new changes to the online repository or [Pull](#pulling) updates from the online repository

### Forking
Forking means that you take someone's repository (for example, MRCL's wiki) and make a copy of it. You can do with this copy whatever you want. However, if you want the "original" repository to get updated, you will have to submit a merge- or pull-request

### Committing
A commit is a group of changes that you decide to make permanent. For example, if I would fix a few typos I would make a commit with the title "fixed some typo's on the Rules page".

Or if I would add a translation, I would make a commit with "Added a Dutch translation of the Credits".

It's considered good practice to make commits as "atomic" as possible, meaning that they should have one singular theme, rather than changing a lot of things at the same time.

### Pushing
Pushing is the action you do when you want to update the online repository with your new commits. 

### Pulling
Pulling is the action you do when you want to update your local files with new commits from the online repository.

### Merge/Pull requests
A merge request (or a pull request, depending on which site you're on) is a request where you ask someone to add your commits to their repository. They can then decide whether or not your contribution is worthy of being added.

### Branches (optional)
A branch is a specific version of the code in a repository. Often, it's considered good practice to work on one feature per branch. For example, if I want to add a guide on contributing, I'll make a contributing branch. But I can also make a translation branch to work on translations. Later, I can merge request them to the 'master' branch, and the changes will be automatically merged where possible.

*tip*: in VSCode you can make a new branch or switch branches by clicking the name of the current branch in the bottom left corner.

## Forking the repository
First, make an account on [Gitlab](www.gitlab.com)

Then, go to our [wiki repository](www.gitlab.com/mrcl1/wiki)

[Fork](#Forking) the repository by clicking this button:

![Fork button](https://i.imgur.com/yyQPEc0.png)

When asked to select a namespace, click the 'select' button under your name. This will make a copy of our repository on your account.

## [Cloning](#cloning) the forked repository
Now we can download our fork onto our computer.
We can do this by clicking the "clone" button, and then copying the "HTTPS" section:

![Clone button](https://i.imgur.com/vlaz8EG.png)

Now we open Visual Studio Code.
Click the Source Control button in the toolbar on the left:

![Toolbar](https://i.imgur.com/cLx9jaG.png)

Then click 'Clone Repository' and paste the link we copied from gitlab. Then, choose where you want to store the repository on your computer.

# Making changes 
Now we can actually do some work.

## Markdown
The wiki is formatted in Markdown.
It's fairly simple to learn and you can find a dedicated guide to it [here](https://www.markdownguide.org/basic-syntax/).

It's also recommended to have the preview tab open, which you can enable with this button:

![Preview button](https://i.imgur.com/QhET3pU.png)

## Auto save
It's recommended to use the auto-save feature. You can toggle this in the File menu on the top left.
In that drop down you'll find 'auto save'. 

## [Committing](#comitting)
Now that we've made our changes, we're ready to commit them.

In the Version Control tab we used earlier, you will now see a list of changes that you've made:

![unstaged changes](https://i.imgur.com/0onT132.png)

To select all the changes, hover over the 'changes' dropdown, and click the '+' Icon.

You can also choose to select individual files, or you can even revert your changes back to the previous commit.

These changes are now 'staged'. This means that we can put these changes in a new commit.

### Good commit messages
A good commit message briefly explains what changes were made. It's important to keep the changes around the same topic.
Examples: 
* Translated the Rules page to Dutch
* Fixed French translation errors
* Improved Rules formatting

You can write your commit message in the box on the top of this toolbar. Once you're satisfied, click the checkmark at the top.

You've now committed your changes.
If you want to make multiple commits (Like one to fix typo's, one to add a translation), you can now make more changes, stage them, and commit them.

Once you're done making commits, we can now upload them to our fork.

# Uploading changes
Now that we've commited our work, we're ready to upload it to our fork, and then merge request it to the original repository.

## Pushing to our fork
### Configuring git
To push our commits, we first need to tell git who we are. Open a terminal by clicking "terminal" in the top toolbar, and then "new terminal".

In the new window type these commands:

``git config --global user.name "mygitlabUsername"``

``git config --goblal user.email "my@email.com"``

Keep in mind that this information will be added to each commit, so use an email adress you don't mind being semi-public. It doesn't have to be the same email adress that you used for gitlab, however.
### Pushing and Pulling
In the bottom left of your VSCode screen, you will see the word 'master', which is the name of your [branch](#branches-optional). On the left of that there will be either a cloud icon, or two arrows. 

Click this icon to push your changes and/or pull commits from the repository.

*Sidenote:* the 'up' arrow indicates the ammount of commits you will push, and the 'down' arrow indicates the ammount of commits you will pull.

## Merge requesting
Now we want to have the original repository accept our changes. To do this, we will create a merge request.

To do this, got to the [Merge Requests](https://gitlab.com/mrcl1/wiki/-/merge_requests) section of the original repository, and click 'new merge request'.

You will get this screen:

![Merge Request Create Screen](https://i.imgur.com/ZGeVWcp.png)

On the left-hand side, we have where we are merging *from*, and on the right-hand side, we have where we are merging *to*. In each box, we have the repository on the left, and the branch on the right.

Make sure that the left dropdown of the left box is set to ``yourusername/wiki``. 

Then, press continue.

On the next screen you'll see a title and a description box. The description box should contain a template that you can fill out.

Please do your best to provide an accurate title and description.

Finally, press 'submit merge request'.

### "oh no, I forgot to add something!"
Don't worry. If you push commits to the branch you're merge-requesting, those commits will be automatically added. The owners of the original repository can also add commits to your Merge Request.
# Updating our fork
Okay, we've gotten our commits merged into the original repository. But how do we update our fork when the original repository updates? 

## The easy "Gitlab virgin" way
This is the most convenient way if you don't want to deal with git too much. Gitlab has a good [Guide](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/) on how to keep your fork automatically updated.

This will automatically update your fork every hour. If you want to manually update your fork, read the next chapter. 

## The "Git Chad" way (optional)

This is how you would do it with any git repository, regardless of where it's hosted.

### Remotes
A remote in git is a link to an online repository. Usually the remote "origin" is your own repository or your fork.

But we don't want to pull changes from our fork, we want to pull changes from the **upstream** repository. In other words, the original repository.

### Adding a remote
First, we need to open a terminal. You can do this in VSCode by clicking "Terminal" at the top and then clicking "new terminal".

Now, we need to get the url to the upstream repository. To do this, we click the "clone" button on the original repository, just like what we did with our fork, and we copy it.

To add this repository as our remote we use this command:
``git remote add upstream URL_HERE``

### Pulling from the remote
Now we can load the upstream changes with ``git fetch upstream`` and then update our local files with ``git pull upstream``

### Updating your online fork
To update your fork on gitlab, we now just have to [Push](#pushing-and-pulling) the changes like we did before. Or, if you want to be a real h@x0rman, use the commandline: ``git push``