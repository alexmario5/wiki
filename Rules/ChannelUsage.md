[Français](/Translated/French/Rules/ChannelUsage.md) | [Español](/Translated/Spanish/Rules/ChannelUsage.md) | [Romanian](/Translated/Romanian/Rules/ChannelUsage.md)

---
# Channel Usage
Here you can find what channels serve what purpose in our [Discord Server](https://discord.mrcl.cc).

**If you want to speak a non-english language, click [here]() to find the channels for that**

# General category
### \#general
The general channel is meant for more serious conversation about serious topics. Shitposting is not allowed, and memes are only allowed if on-topic. There is no hard inforcement here, just don't post a random meme while people are having a serious conversation.

### \#sewers
Sewers is meant for shitposting and memes, or random 'hey this reddit post is pretty cool' links. This doesn't mean you're allowed to spam, but you can be a bit more loose in this channel.

### \#bot-spam
Bot-spam is meant to play around with bots. This channel will not be monitored as much, so be sure to call in a moderator if people misbehave. That being said, this channel is rather spammy by nature, so the 'no-spam' rule is only loosely enforced.

### \#voice-chat
This channel is for those who want to interact with people who are voice chatting through text. Don't use music bots here, keep that in #bot-spam

### \#suggestions
This channel is for posting your suggestions for the discord server, or the minecraft server. Keep in mind that your ideas have likely been suggested before, and that we might completely ignore some of them as well. It's just a fun place to brainstorm.

# Language channels
At MRCL, we're open to people who speak different languages. We currently have these channels available:
- \#fr-talk
- \#es-talk
- \#fi-talk

These channels are either for speaking the respective languages, or asking help with these languages (eg: "How do I say 'bageuette' in french?")

If you want a channel for your language, contact and admin and we'll consider it.

# Misc category
Not all the channels in this category will be featured here, just the ones with specific rules

### \#head-area-exposing-channel
This is a channel for face-reveals. Don't post nudes of yourself (or of anyone) and don't post fake face reveals. It's just not funny. **don't face-reveal someone else** either.

### \#not-safe-for-kids
**Porn and nudity is not allowed in this channel**

NSFK is a channel where you can post your darker memes or content that is generally not appropriate for, say, 14yr olds. You can also talk about more sensitive topics like sexuality, politics or gender here. (note that these topics are not banned in other channels per-se, this one is just the dedicated channel.)

### \#mrclmemes
This channel is for **memes only**. 
Do **not** comment on the memes posted here. This is just to archive all the MRCL-related memes in one channel.

### \#free-games
This channel is where you can post games who are **free-to-keep** for a limited time. We have a bot that occasionally posts links here as well. (we're not responsible for the content the bot posts, fyi)

### \#screenshots
This channel is where you can post your beautiful MRCL screenshots. **Commentary is not allowed.**
This is also not the channel to report bugs, that would be #bug-report.

If you like a screenshot, you can react with the :pushpin: emoji, and we'll pin it if enough people feel the same way.

We might use these screenshots on our site or other media as well.
