[Français](/Translated/French/Rules/Rules.md) | [Español](/Translated/Spanish/Rules/rules.md) | [Romanian](/Translated/Romanian/Rules/Rules.md)

---
# Rules
The rules in this document apply to the discord and the minecraft server.
They are a guideline, and in the end Admins and Moderators have the final call.
They're also listed in no particular order, except The First Rule

## The First Rule
The first and most important rule is: **use common sense**. 

Don't be a nuisance to others, and be a decent human being. Don't make us add a rule for your sake just because we overestimated human decency.

## Global rules

- Moderation decisions are final. If you wish to complain, contact an admin, or Jack if you wish to complain about an admin.

- **Telling anyone where or how to achieve an advancement is not allowed.** You can explain normal game mechanics, however. A full list of what is and isn't allowed can be found [here](AdvancementRules.md)

- Spam is not allowed. Depending on the channel, this rule is enforced differently. See [Channel Usage](ChannelUsage.md) for more details.

- You may argue over just about anything, but we draw the line at personal insults. If you want to hold a roast fest, do it in DM's

- Racism and discrimination of any kind is not something we tolerate. 

- We have no bans on swearing, but use your common sense and don't overdo it.

- You may **only** advertise Code Lyoko related projects within reason. This is up to moderator discretion. If you wish to advertise some other project, ask an admin first.

- **We have restrictions on usernames and nicknames, which you can read about [here](NamingRules.md)**

- The common language is English, but other languages may be spoken in-game and in [specific channels](ChannelUsage.md#Language_channels)

- Alt accounts are not allowed.

- NSFW content like nudity or porn is not allowed. Jokes and memes are allowed to a certain degree in the appropriate [channels](ChannelUsage.md)

- Ban evasion will get you...banned again. Permanently. Seems pretty logical yet I have to write this rule.

- **Being a dick to other people in DM's or other servers might also get you banned here**
## Discord-specific rules

- Exessively mentioning/@'ing anyone without cause will get you muted, or worse.

- Trolling/annoying people with bots is not allowed. If someone complains, you will always be in the wrong. 

## Minecraft-specific rules
- Trolling by using game mechanics is not allowed. Examples include, but are not limited to: 
    * Changing the scanner destination of someone without asking
    * Spamming the Return To The Past
    * Repeatedly pvp-ing against someone's will while not xanafied
