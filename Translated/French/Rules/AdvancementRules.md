[English](/Rules/AdvancementRules.md) | [Español](/Translated/Spanish/Rules/AdvancementRules.md) | [Romanian](/Translated/Romanian/Rules/AdvancementRules.md)

---
# Règles des Succès (Advancement)
Ici, vous pouvez trouver plus d'explications à propos de la règle qui dit que vous pouvez pas expliquer aux gens comment et où avoir un succès.

En général, vous pouvez pas dire à quelqu'un comment faire un succès du style : "Ah oui, le journal de Nathan est à (ses coordonnées)" ou "Regarde dans (ce bâtiment)".

## Travaillez ensemble
Si vous et vos ami(e)s veulent faire un succès ensemble vous pouvez **SI** aucun d'entre vous à fait le succès (Vous avez pas le droit d'emmener quelqu'un à un succès).

En plus de cela, vous pouvez pas partager la recherche, vous devez être ensemble. Vous avez pas le droit de demander à votre ami(e) va regarder l'endroit A pendant que je vais à l'endroit B. Pour faire court : **restez ensemble** et si un de vos ami(e)s trouvent quelque chose sans faire exprès en étant seul(e), ils peuvent pas vous dire où c'est.

## Exceptions

### Gameplay
**Règle générale : vous pouvez expliquer le gameplay aux autres joueurs.**

Ça veut dire que vous pouvez expliquer aux gens des actions qui sont liés au gameplay du serveur. Ces actions peuvent ammener sur des succès, donc elles sont exclus de la règle. 

Vous pouvez trouver la liste [ici](#Liste)

### Succès en co-op
Il y a aussi des exceptions pour les succès qui demandent plus de un joueur pour être complété.

Bien sûr, vous pouvez demander à des joueurs de vous rejoindre pour ces succès. Vous êtes encouragés à ne pas spoil/révéler la nature précise de ces succès pour qu'elles restent intéressantes pour les autres.

### Liste
C'est la liste complète de tout les succès que vous pouvez expliquer que ça soit à cause du gameplay ou parce que c'est en co-op.

**Il se peut que la liste ne soit pas forcément à jour, si vous avez un doute demandez à un administrateur.**
- Kadic
    * Tu comptes fini ça ?
    * Il faudrait plus de sauce...
    * Patates pour le dessert
    * L'homme le plus gentil
    * Amoureux des ordinateurs
    * R.I.P.
    * Pourquoi c'est gris ?
- Usine
    * Déjà Vu
- Lyoko
    * Tous (dans l'onglet 'Lyoko')

**Note: Aucun succès du 5ème Territoire font partie de l'exception.**

## Sanctions
Si on voit que vous aidez d'autres personnes avec les succès alors l'une de ces sanctions pourraient arriver :
* Avertissement
* Exclus du grade Vétéran
* Banni temporairement
* Banni pour toujours

En général, ces sanctions sont données dans l'ordre, sauf si vous faites exprès de pas respecter les règles.