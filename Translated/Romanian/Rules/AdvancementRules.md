[English](/Rules/AdvancementRules.md) | [Français](/Translated/French/Rules/AdvancementRules.md) | [Español](/Translated/Spanish/Rules/AdvancementRules.md)

---
# Regulile Advancementurilor
Aici poti gasi mai multe informatii despre aceasta regula : Nu poti spune la nimeni unde sau cum sa gaseasca un advancement.

In general, nu poti spune cuiva cum sa completeze un advancement, de exemplu "Oh da, jurnalul lui Nathan este la (aceste coordonate)" sau "Uitate in (aceasta cladire) si o sa il gasesti"
## Lucrand impreuna
Daca tu si prietenii tai doriti sa completati un advancement impreuna, aveti voie sa lucrati impreuna **doar daca** nici unul dintre voi nu ati completat acest advancement inainte. (Nu poti sa conduci pe cineva la un advancement) 

In plus, nu puteti sa impartiti munca. Trebuie sa cautati advancementurile impreuna, nu va puteti despartii. Nu poti sa ii spui prietenului tau sa se uite in punctul A intre timp ce tu te uiti la punctul B. Mai pe scurt **trebuie sa ramaneti impreuna** si daca unul dintre prietenii tai gaseste ceva din greseala cand este singur, nu are voie sa iti zica unde a gasit acel ceva.

## Exceptii

### Gameplay
**Regula generala: Poti explica mechanicile jocului altor jucatori**

Asta inseamna  ca poti explica altor jucatori cum sa faca o anumita actiune. Aceste actiuni pot conduce spre un advancement, asa ca sunt exceptie de la aceasta regula.

Gaseste lista [aici](#Lista)
### Advancementurile co-op
Totusi este o exceptie pentru advancementurile care cer mai mult de o persoana sa fie completate.

Totusi poti, desigur, sa intrebi alti jucatori sa vina cu tine pentru a completa advancement. Esti incurajat sa nu spui tot ce se va intampla in acel advancement, pentru a fii interesant si pentru ceilalti jucatori.

### Lista
Acesta este o lista cu toate advancementurile care au voie sa fie explicate, pentru ca sunt ori co-op ori sunt mechanicile jocului.

**Aceasta lista ar putea fi invechita, asa ca poti intreba un admin cand nu esti sigur**
- Kadic
    * Are you going to finish that?
    * Gravy... It needs more gravy
    * Lamb and potatoes for dessert
    * The Goodest Boy
    * Windows lover
    * R.I.P.
    * But why is it gray?
- Factory
    * Déjà Vu
- Lyoko
    * Toate (Din pagina Lyoko)

**Tine minte ca nici unul dintre advancementurile din Sector 5 nu sunt scutite**


## Pedepse
Daca esti prins ca ii ajuti pe altii cu advancementurile unele dintre urmatoarele lucruri s-ar putea intampla; ai putea fi
* Avertizat
* Exclus din primirea statusul de Veteran
* Banat temporar
* Banat permanent

Deobicei aceste pedepse vor fi in aceasta ordine, dacă nu încalci în mod intenționat regulile.