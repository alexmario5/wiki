[English](/Rules/NamingRules.md) | [Français](/Translated/French/Rules/NamingRules.md) | [Español](/Translated/Spanish/Rules/NamingRules.md)

---
# Reguli de nume
Avem niște reguli foarte specifice pentru nume și pe discord și pe minecraft.

## Nume care nu sunt permise
Nu poți să ai un nume care:
* Resemblă un nume a unui caracter din serial
* Conține doar litere neidentificabile
* Nu poate fi mentionat ca și cum ar fi un nume
* Nu poate fi mentionat usors pe discord

## Ce pot face dacă am un nume care nu este permis
Depinde. Aici sunt cateva scenarii:

### Numele interzis este numele de pe minecraft
Daca ai o copie de minecraft cumparata, și nu vrei să îți schimbi numele de pe mojang.com, intreaba un moderator să iti schimbele numele de pe server in altceva.

**Dacă nu ai minecraft cumpărat, schimbăți numele de pe launcher sau vei fi banat**

### Numele interzis este numele de pe discord
Daca nu vrei să iti schimbi numele de pe discord, foloseste functia de nickname (sau o să o facem noi pentru tine, dar nu o să iti placa ce nume alegem).

## Exemple de nume interzise
### 11ε ƉơƈŧēůƦ
Nu este permis deoarece nu are caractere normale.
### Aelita_Shaeffer
Nu este permis deoarece este numele unui caracter din CL.
### Jeremie
La fel ca mai sus. Dacă numele tau este chiar Jeremie, contactează un moderator.
### gksqzeoKKSdqslAZRf 
Nu este permis deoarece nu poate fi folosit ca un nume. Nu poți sa te astepti sa zicem "Hey, gksqzeoKKSdqslAZRf ce mai faci?"