[English](/Rules/Rules.md) | [Français](/Translated/French/Rules/rules.md) | [Romanian](/Translated/Romanian/Rules/rules.md)

---
Please help us translate this page!
Contact us on discord for help on how to work with git, or submit a merge request if you know how!