[English](/Rules/AdvancementRules.md) | [Français](/Translated/French/Rules/AdvancementRules.md) | [Romanian](/Translated/Romanian/Rules/AdvancementRules.md)

---
Please help us translate this page!
Contact us on discord for help on how to work with git, or submit a merge request if you know how!